CREATE TABLE posts (
	`id` integer PRIMARY KEY NOT NULL,
	`user_id` integer,
	`prid` integer,
	`title` text NOT NULL,
	`message` text NOT NULL,
	FOREIGN KEY (`user_id`) REFERENCES users(`id`),
	FOREIGN KEY (`prid`) REFERENCES posts(`id`)
);
