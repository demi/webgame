/* eslint-disable */
import esbuild from 'esbuild'
import crypto from 'crypto'
import fs, { emptyDir } from 'fs-extra'
import path from 'path'
import html from '@chialab/esbuild-plugin-html'
import { fileTypeFromFile } from 'file-type'
import { brotliCompress, gzip, constants as zlibConstants } from 'zlib'
import child_process from 'child_process'
import { createRequire } from 'module'
import { solidPlugin } from 'esbuild-plugin-solid'

// css stuff just for the index.html to load faster.
import { PurgeCSS } from 'purgecss'
import postcss from 'postcss'
import variableCompress from 'postcss-variable-compress'
import tailwindcss from 'tailwindcss'
import daisyui from 'daisyui'
import cssnano from 'cssnano'

const require = createRequire(import.meta.url)

let args = process.argv.slice(2)

const prod = args[0] == 'prod'
const minify = prod

// Replace node path, fs, etc for using @babel/core in browser.
const replaceNodeBuiltIns = () => {
  const noop = require.resolve('./src/noop.js')
  const replace = {
    path: noop,
    fs: noop,
    module: noop,
    // assert: noop,
    // util: noop,
    // url: noop,
  }
  const filter = RegExp(`^(${Object.keys(replace).join('|')})$`)
  return {
    name: 'replaceNodeBuiltIns',
    setup(build) {
      build.onResolve({ filter }, (arg) => ({
        path: replace[arg.path],
      }))
    },
  }
}

let compressBrotli = (buf) => {
  return new Promise((resolve, reject) => {
    // gzip(
    brotliCompress(
      buf,
      {
        params: {
          [zlibConstants.BROTLI_PARAM_QUALITY]: prod ? 11 : 9,
          [zlibConstants.BROTLI_PARAM_MODE]: zlibConstants.BROTLI_MODE_TEXT,
        },
      },
      (error, result) => {
        if (error) reject(error)
        else resolve(result)
      }
    )
  })
}
let compressBrotliFile = async (file, addExtension = true) => {
  let out
  if (addExtension) out = `${file}.br`
  else out = file
  console.log('compress brotli', out)
  const buf = await fs.readFile(file)
  const compressed = await compressBrotli(buf)
  return fs.writeFile(out, compressed)
}

function* walkSync(baseDir, relDir = '') {
  const dir = path.join(baseDir, relDir)
  const files = fs.readdirSync(dir, { withFileTypes: true })
  for (const file of files) {
    if (file.isDirectory()) {
      // if (file.name != 'assets')
      yield* walkSync(baseDir, path.join(relDir, file.name))
    } else if (file.name == '.DS_Store') {
      fs.rmSync(`${relDir}/${file.name}`)
    } else if (
      !file.name.startsWith('_') &&
      file.name != 'robots.txt' &&
      file.name != 'sw.js' &&
      file.name != 'index.html' &&
      !file.name.endsWith('.map') &&
      !file.name.endsWith('.png')
    ) {
      yield path.join(relDir, file.name)
    }
  }
}

const resolve = (path) => `./build/${path}`

let buildDate = new Date()
async function packArchive(metaFile) {
  // Rebuild sw.js and _worker.js in separate build context.
  const workerPromise = workerContext.rebuild()

  // Delete .DS_store
  ;[...walkSync('./')]

  const cacheId = crypto.randomUUID()
  // const cacheName = `${cacheId}.tar.gz`
  // const cacheName = `${cacheId}.tar`
  // const cacheName = `${cacheId}.txt`

  // Inline Daisy UI css into html to avoid FOUC on firefox.
  if (true) {
    let path = resolve('game.html')
    let game = (await fs.readFile(path)).toString()

    const regex = /<link.*href="(_daisyui.*?)".*>/

    const result = game.match(regex)
    const css = (await fs.readFile(resolve(result[1]))).toString()

    await fs.writeFile(
      path,
      game.replace(regex, `<style type="text/css">${css}</style>`)
    )
  }

  console.log('packArchive', process.cwd())

  // Copy over files from public/ as-is
  // TODO: re-enable for webgpu support. Currently webgpu broken on chrome-dev
  // await fs.copy('public', 'build')
  await fs.copy('public/_headers', 'build/_headers')
  await fs.copy('public/_routes.json', 'build/_routes.json')
  await fs.copy('public/robots.txt', 'build/robots.txt')
  await fs.copy('src/mods/', 'build/mods/')

  const allFiles = [...walkSync('build')]

  let fileType = async (path) => {
    // file-type doesn't handle text type formats, only binary.
    if (path.endsWith('.js')) {
      return { ext: 'js', mime: 'text/javascript' }
    } else if (path.endsWith('.html')) {
      return { ext: 'html', mime: 'text/html' }
    } else if (path.endsWith('.svg')) {
      return { ext: 'svg', mime: 'image/svg+xml' }
    } else if (path.endsWith('css')) {
      return { ext: 'css', mime: 'text/css' }
    } else {
      return await fileTypeFromFile(path)
    }
  }

  const cacheFiles = (
    await Promise.all(
      allFiles.map(async (path) => {
        const resolved = resolve(path)
        const stat = await fs.stat(resolved)
        return { path, size: stat.size, ...(await fileType(resolved)) }
      })
    )
  ).sort((a, b) => b.size - a.size)
  console.log(cacheFiles)

  const cacheMap = {}
  let cacheOffset = 0
  for (let entry of cacheFiles) {
    const { path, ...notPath } = entry
    notPath.offset = cacheOffset
    cacheOffset += notPath.size
    cacheMap[path] = notPath
  }

  // Hack to resolve import map urls while esbuild doesn't support import.meta.url
  // Remove once this is merged.
  // https://github.com/evanw/esbuild/pull/2508
  if (true) {
    const [path, _] = Object.entries(metaFile.outputs).find(
      ([path, v]) => v.entryPoint == 'src/_loader.ts' && fs.existsSync(path)
    )
    console.log(`Resolving import urls for ${path}...`)

    let loader = (await fs.readFile(path))
      .toString()
      .replace('__CACHE_MANIFEST__', JSON.stringify(cacheMap))

    const regex = /:[\s]?import\("(.*?)"\)/g
    loader = loader.replaceAll(regex, ':new URL("$1",location.origin).pathname')

    await fs.writeFile(path, loader)
  }

  // Replace __CACHE_ARCHIVE__ in index.html, define: doesn't work there for some reason.
  if (false) {
    let path = resolve('index.html')
    let index = (await fs.readFile(path)).toString()
    const archive = JSON.stringify(`assets/${cacheId}.br`)

    const regex = /<script.*src="(.*?)".*><\/script>/
    const result = index.match(regex)
    const script = (await fs.readFile(resolve(result[1])))
      .toString()
      .replace('__CACHE_MANIFEST__', JSON.stringify(cacheMap))
      .replace('__CACHE_ARCHIVE__', archive)
      .replaceAll('import.meta.url', 'location.origin')
    // Hack to let _loader.ts to work as a non-module since import.meta
    // is only available inside modules

    await fs.writeFile(
      path,
      // index.replace(regex, `<script type="module">${script}</script>`)

      // NOTE: type="module" can't dynamically modify import-map
      // since import-map can't be injected after module load.
      index.replace(regex, `<script>${script}</script>`)
    )
  }

  // daisyui + tailwind + postcss + purgecss just for the first page.
  // Other pages will use twind and the full cdn version of daisyui
  if (true) {
    let path = resolve('index.html')
    let index = (await fs.readFile(path)).toString()
    const archive = JSON.stringify(`assets/${cacheId}.br`)
    const regex = /<script.*src="(.*?)".*><\/script>/
    const result = index.match(regex)
    const script = (await fs.readFile(resolve(result[1])))
      .toString()
      .replace('__CACHE_ARCHIVE__', archive)
    // Hack to let _loader.ts to work as a non-module since import.meta
    // is only available inside modules

    // Split instead of replace since script has regex in it, and that
    // seems to interfere with the replace method.
    const split = index.split(result[0])

    let out
    if (true) {
      const cssRegex = /<link.*rel="stylesheet".*href="(.*?)".*>/
      const cssResult = split[0].match(cssRegex)

      let cssPath = resolve(cssResult[1])

      let { css } = await postcss([
        tailwindcss({
          content: [resolve('index.html'), resolve(result[1])],
          plugins: [daisyui],
          minify: prod,
          daisyui: {
            darkTheme: false,
            lightTheme: false,
            themes: ['light', 'dark'],
          },
        }),
        ...(prod
          ? [
              variableCompress(['--glass-reflex-opacity']),
              cssnano({ preset: 'advanced' }),
            ]
          : []),
      ]).process((await fs.readFile(cssPath)).toString(), {
        from: cssPath,
      })
      const cssPath2 = resolve('_hello.css')
      fs.writeFileSync(cssPath2, css)

      const purged = await new PurgeCSS().purge({
        css: [cssPath2],
        content: [resolve('index.html'), resolve(result[1])],
      })
      css = purged[0].css

      out =
        split[0].replace(cssRegex, `<style>${css}</style>`) +
        '<script>(()=>{' +
        script +
        '})()</script>' +
        split[1]
    } else {
      out =
        split[0] + '<script type="module">' + script + '</script>' + split[1]
    }

    await fs.writeFile(
      path,
      // index.replace(regex, `<script type="module">${script}</script>`)

      // NOTE: type="module" can't dynamically modify import-map
      // since import-map can't be injected after module load.
      // index.replace(regex, `<script type="module">${script}</script>`)
      out
    )
  }

  if (args[0] == 'keep' || args[1] == 'keep') return

  const cacheBuffer = Buffer.alloc(cacheOffset)
  await Promise.all(
    Object.entries(cacheMap).map(async ([path, info]) => {
      let buf = await fs.readFile(resolve(path))
      if (buf.copy(cacheBuffer, info.offset) != info.size) {
        console.error(`File size mismatch for ${path}`, info)
      }
    })
  ).then(() => {
    return compressBrotli(cacheBuffer).then((compressed) => {
      return fs.writeFile(resolve(`assets/${cacheId}.br`), compressed)
    })
  })
  const cacheArchiveStats = await fs.stat(resolve(`assets/${cacheId}.br`))
  console.log(
    `Brotli compressed ${(cacheOffset * 1e-6).toFixed(2)} MB -> ${(
      cacheArchiveStats.size * 1e-6
    ).toFixed(2)} MB`
  )
  // NOTE: Content-Type needs to be br, for example in dist/_headers for
  // cloudflare pages, or in cloudflare transforms.

  // Serve source maps from web instead of cache to reduce cache archive size.
  const mapFiles = fs.readdirSync('build/').filter((f) => f.endsWith('.map'))

  if (true) {
    if (await fs.exists('dist')) {
      await fs.emptyDir('dist')
    } else {
      await fs.mkdir('dist')
    }

    await workerPromise

    await Promise.all([
      fs.copy('build/assets', 'dist/assets'),
      compressBrotliFile(resolve('index.html'), false),
      compressBrotliFile(resolve('sw.js'), false),
      compressBrotliFile(resolve('_loader.js'), false),
      ...mapFiles.map((f) => {
        return compressBrotliFile(resolve(f), false)
      }),
    ])

    const files = [
      'index.html',
      'sw.js',
      '_loader.js',
      '_worker.js',
      // `${cacheId}.br`,
      '_headers',
      '_routes.json',
      'robots.txt',
      ...mapFiles,
    ]
    await Promise.all(
      files.map((path) => fs.copyFile(resolve(path), `dist/${path}`))
    )
  }

  await fs.emptyDir('build')
}

/** @type {esbuild.BuildOptions} */
const buildOpts = {
  logLevel: 'info',
  bundle: true,
  metafile: true,
  target: 'esnext',
  format: 'esm',
  outdir: 'build',

  minify,
  treeShaking: true,

  sourcemap: true,

  // Enable code splitting for (new URL(..., import.meta.url))
  splitting: true,

  plugins: [
    solidPlugin({
      solid: {
        // disable delegateEvents since it interferese with dynamically
        // loaded solid-js components in main.tsx, etc.
        delegateEvents: false,
      },
    }),
    html(),
    {
      name: 'pack',
      setup(build) {
        build.onEnd(async (result) => {
          if (result.metafile.outputs['build/index.html']) {
            // Final build step finished, files available.
            buildDate = new Date()
            return packArchive(result.metafile)
          }
        })
      },
    },
  ],

  entryNames: '[name]',
  assetNames: 'assets/[name]-[hash]',
  chunkNames: '[name]-[hash]',
  // chunkNames: '[ext]/[name]-[hash]',
  // assetNames: 'assets/[name]',
  // chunkNames: '[ext]/[name]',

  entryPoints: [
    'game.html',
    'index.html',
    'src/main.tsx',
    'src/_loader.ts',
    'src/_hello.tsx',
  ],
  loader: { '.html': 'copy' },
  pure: prod ? ['console.log', 'console.warn', 'console.error'] : [],

  define: {
    CACHE_NAME: JSON.stringify('my_cache'),
    DEVELOPMENT: JSON.stringify(!prod),
  },

  // banner: {
  //   js: "(() => { (new EventSource(\"/esbuild\")).addEventListener('change', () => location.reload()); })();"
  // },
}
let workerContext
async function buildOrServe() {
  const context = await esbuild.context(buildOpts)

  /** @type {esbuild.BuildOptions} */
  const workerOpts = {
    logLevel: 'info',
    bundle: true,
    metafile: true,
    target: 'esnext',
    format: 'esm',
    outdir: 'build',

    minify,
    treeShaking: true,
    sourcemap: false,
    splitting: false,

    entryNames: '[name]',

    entryPoints: ['src/_worker.ts', 'src/sw.ts'],
    plugins: [replaceNodeBuiltIns()],

    define: {
      __SW_VERSION__: JSON.stringify(buildDate),
      CACHE_NAME: JSON.stringify('my_cache'),
      DEVELOPMENT: JSON.stringify(!prod),
      'process.env.BABEL_TYPES_8_BREAKING': JSON.stringify(true),
      'process.env.BABEL_ENV': JSON.stringify('development'),
      'process.env.NODE_DEBUG': JSON.stringify(false),
      'process.platform': JSON.stringify('browser'),
      'Buffer.isBuffer': '"(v) => false"',
    },
  }

  workerContext = await esbuild.context(workerOpts)

  await fs.emptyDir('build')

  if (args[0] == 'serve' || args[1] == 'serve') {
    let die = async () => {
      await context.dispose()
      await workerContext.dispose()
      process.exit()
    }
    process.on('SIGINT', die) // CTRL+C
    process.on('SIGQUIT', die) // Keyboard quit
    process.on('SIGTERM', die) // `kill` command

    const wrangler = child_process.spawn('pnpm', ['db'])
    wrangler.stdout.on('data', (data) => {
      console.log(data.toString())
    })
    wrangler.stderr.on('data', (data) => {
      console.error(data.toString())
    })
    wrangler.on('exit', (code) => {
      console.log('wrangler terminated with code ', code)
    })

    const caddy = child_process.spawn('caddy', ['run'])
    caddy.stdout.on('data', (data) => {
      console.log(data.toString())
    })
    caddy.stderr.on('data', (data) => {
      console.error(data.toString())
    })
    caddy.on('exit', (code) => {
      console.log('Caddy terminated with code ', code)
    })

    let stdin = process.openStdin()
    let isRebuilding = false
    stdin.on('data', (data) => {
      if (data.length == 1 && data[0] == 0x0a && !isRebuilding) {
        isRebuilding = true
        context.rebuild().then(() => (isRebuilding = false))
        console.log('Rebuilding...')
      }
    })
    stdin.resume()

    // Serve only for live reload
    await context.serve({
      port: 7777,
    })

    // The return value tells us where esbuild's local server is
    return await context.watch()
    // return await waitForDie
  } else {
    await context.rebuild()
    return Promise.all([workerContext.dispose(), context.dispose()])
  }
}

buildOrServe().catch((error) => {
  console.error(error)
  process.exit(1)
})
