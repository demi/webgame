import { createSignal, ParentComponent } from 'solid-js'

const Counter: ParentComponent = (props) => {
  const [count, setCount] = createSignal(0)
  const [timer, setTimer] = createSignal(0)

  setInterval(() => setTimer(timer() + 1), 1000)

  return (
    <>
      <div class="card glass pointer-events-auto m-4 mt-16 w-80 shadow-2xl">
        <div class="card-body">
          <h1 class="card-title m-auto">Info</h1>
          <div>Timer: {timer}</div>
          {props.children}
          <div class="card-actions justify-end">
            <button
              type="button"
              class="btn btn-secondary"
              onClick={() => setCount(count() + 1)}
            >
              Count: {count}
            </button>
          </div>
        </div>
      </div>
    </>
  )
}

export default Counter
