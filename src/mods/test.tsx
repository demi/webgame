import { createSignal } from 'solid-js'

// TODO: getPreferredCanvasFormat on firefox
// https://github.com/webcompat/web-bugs/issues/83405
// const supportsWebGPU =
//   'gpu' in navigator && 'getPreferredCanvasFormat' in navigator.gpu!

interface FpsInfo {
  fps: number
  avgFps: number
  avgFrametime: number
}

const [engineName, setEngineName] = createSignal('')
const [info, setInfo] = createSignal<FpsInfo>({
  fps: 0,
  avgFps: 0,
  avgFrametime: 0,
})

export async function start(canvas: HTMLCanvasElement) {
  // Dynamic import babylon to make use of import split in vite.
  const {
    Engine,

    // TODO: Re-enable once working in chrome-dev again.
    // WebGPUEngine,

    Scene,
    // FreeCamera,
    ArcRotateCamera,
    Vector3,
    Color3,
    Color4,
    HemisphericLight,
    DirectionalLight,
    ShadowGenerator,
    MeshBuilder,
    StandardMaterial,
  } = await import('@babylonjs/core')

  // Generate the BABYLON 3D engine
  const engine = new Engine(canvas, true)
  // const engine = supportsWebGPU
  //   ? await (async () => {
  //       const e = new WebGPUEngine(canvas, {
  //         antialiasing: true,
  //       })
  //       // TODO: install glslang/twgsl from npm (not available yet?) or as subprojects
  //       await e.initAsync(
  //         {
  //           jsPath: '/babylon/glslang.js',
  //           wasmPath: '/babylon/glslang.wasm',
  //         },
  //         {
  //           jsPath: '/babylon/twgsl.js',
  //           wasmPath: '/babylon/twgsl.wasm',
  //         }
  //       )
  //       return e
  //     })()
  //   : new Engine(canvas, true)

  setEngineName(engine.name)

  const createScene = function () {
    // Creates a basic Babylon Scene object
    const scene = new Scene(engine)
    scene.clearColor = Color4.FromInts(0, 0, 0, 0)
    // Creates and positions a free camera
    // const camera = new FreeCamera('camera1', new Vector3(0, 5, -10), scene)
    const camera = new ArcRotateCamera(
      'camera1',
      -Math.PI / 2,
      Math.PI / 3,
      10,
      new Vector3(0, 0, 0),
      scene
    )
    camera.upperBetaLimit = Math.PI / 2
    camera.lowerRadiusLimit = 5.0
    camera.upperRadiusLimit = 50.0
    camera.useAutoRotationBehavior = true
    if (camera.autoRotationBehavior) {
      camera.autoRotationBehavior.idleRotationSpeed = -1.0
      camera.autoRotationBehavior.idleRotationSpinupTime = 5000
      camera.autoRotationBehavior.idleRotationWaitTime = 1000
    }
    // Targets the camera to scene origin
    // camera.setTarget(Vector3.Zero())

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true)
    // Creates a light, aiming 0,1,0 - to the sky
    const light = new DirectionalLight('light', new Vector3(-1, -1, 0), scene)
    light.position = new Vector3(0, 10, 10)
    // Dim the light a small amount - 0 to 1
    light.intensity = 0.7
    const light2 = new HemisphericLight('light2', new Vector3(0, 0.5, 1), scene)
    light2.intensity = 0.3
    // Built-in 'sphere' shape.
    const sphere = MeshBuilder.CreateCapsule(
      'sphere',
      {
        radius: 0.5,
        height: 3.0,
        subdivisions: 32,
        tessellation: 32,
        capSubdivisions: 32,
      },
      scene
    )
    // Move the sphere upward 1/2 its height
    sphere.position.x = 1.0
    sphere.position.y = 1.5

    const otherSphere = MeshBuilder.CreateSphere(
      'sphere',
      { diameter: 1.5, segments: 32 },
      scene
    )
    // Move the sphere upward 1/2 its height
    otherSphere.position.x = -1
    otherSphere.position.y = 0.75
    // Built-in 'ground' shape.
    const ground = MeshBuilder.CreateGround(
      'ground',
      { width: 6, height: 6 },
      scene
    )

    const basicMaterial = new StandardMaterial('sphere', scene)
    // basicMaterial.diffuseColor = new Color3(0.8, 0.3, 1.0)
    basicMaterial.diffuseColor = Color3.Random()
    // basicMaterial.specularColor = new Color3(0.7, 0.3, 0.5)
    otherSphere.material = basicMaterial

    ground.receiveShadows = true
    sphere.receiveShadows = true
    otherSphere.receiveShadows = true

    const shadowGenerator = new ShadowGenerator(2 * 1024, light)
    shadowGenerator.addShadowCaster(sphere)
    shadowGenerator.addShadowCaster(otherSphere)
    // shadowGenerator.useExponentialShadowMap = true
    // shadowGenerator.usePoissonSampling = true
    return scene
  }
  const scene = createScene() //Call the createScene function
  const camera = scene.getCameraByName('camera1')
  // Register a render loop to repeatedly render the scene
  let frametimes: Array<number> = []
  let fpsUpdate = performance.now()
  const DT_UPDATE = 200
  engine.runRenderLoop(function () {
    scene.render()

    const now = performance.now()
    frametimes.push(now)
    if (now > fpsUpdate) {
      fpsUpdate += (Math.floor((now - fpsUpdate) / DT_UPDATE) + 1) * DT_UPDATE

      const then = now - 5000
      const i = frametimes.findIndex((v) => v > then)
      if (i > 0) frametimes = frametimes.slice(i)

      const avgFrametime =
        now - then > 0 ? (now - frametimes[0]) / frametimes.length : 0
      const avgFps = avgFrametime > 0 ? 1000 / avgFrametime : 0
      setInfo({ fps: engine.getFps(), avgFps, avgFrametime })
    }
  })
  // Watch for browser/canvas resize events
  window.addEventListener('resize', function () {
    engine.resize()
  })
}

export function Info() {
  return (
    <>
      <p>
        Engine: {engineName}
        <br />
        FPS: {info().fps.toFixed(1)} ({info().avgFps.toFixed(1)})
        <br />
        frametime: {info().avgFrametime.toFixed(2)} ms
      </p>
    </>
  )
}
