import type { JSX, ParentComponent } from 'solid-js'

type MyProps = { version: string; themeChanger: JSX.Element }

function NavBar(props: MyProps) {
  return (
    <nav class="navbar lg: pointer-events-auto fixed w-full gap-2 md:gap-1">
      <div class="flex flex-1">
        <span
          class="tooltip tooltip-bottom before:text-xs before:content-[attr(data-tip)]"
          data-tip="Menu"
        >
          <label for="drawer" class="btn btn-square btn-ghost drawer-button ">
            <svg
              width="20"
              height="20"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              class="inline-block h-5 w-5 stroke-current md:h-6 md:w-6"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M4 6h16M4 12h16M4 18h16"
              ></path>
            </svg>
          </label>
        </span>
        <div class="flex items-center gap-2 ">
          <a
            href="/"
            aria-current="page"
            aria-label="Homepage"
            class="flex-0 btn btn-ghost px-2 "
          >
            <div class="font-titletext-primary inline-flex text-lg  transition-all duration-200 md:text-3xl">
              <span class="text-content normal-case">{document.title}</span>
            </div>
          </a>{' '}
          <a
            href="/docs/changelog"
            class="link link-hover font-mono text-xs text-opacity-50 "
          >
            <div class="hidden sm:block">
              <span data-tip="Changelog" class="tooltip tooltip-bottom">
                {props.version}
              </span>
            </div>
          </a>
        </div>
      </div>

      {props.themeChanger}
    </nav>
  )
}

export const Drawer: ParentComponent<MyProps> = (props) => {
  return (
    <div
      class="drawer pointer-events-none h-full w-full"
      style={{
        '--glass-reflex-opacity': 0,
      }}
    >
      <input id="drawer" type="checkbox" class="drawer-toggle" />
      <div class="drawer-content">
        <NavBar {...props} />
        <div class="pointer-events-none">{props.children}</div>
      </div>
      <div class="drawer-side">
        <label for="drawer" class="drawer-overlay pointer-events-auto"></label>
        <aside class="glass card pointer-events-auto w-64 rounded">
          <ul class="menu text-base-content flex flex-col p-0 px-4">
            <li>
              <a>Item 1</a>
            </li>
            <li>
              <a>Item 2</a>
            </li>
            <li>
              <a>Item 3</a>
            </li>
          </ul>
        </aside>
      </div>
    </div>
  )
}
