import { ThemeChange } from '!tsx/ThemeChange'
import Demo from '!tsx/debug'
import { Drawer } from '!tsx/drawer'
import { start, Info } from '!tsx/test'
import themes from '!ts/defaultThemes'
import { Component } from 'solid-js'

export type PageProps = {
  trpcTest: () => void
}

export const Page: Component<PageProps> = (props: PageProps) => {
  const canvas = document.getElementById('game') as HTMLCanvasElement
  start(canvas)

  const themeChanger = <ThemeChange themes={themes}></ThemeChange>
  return (
    <Drawer themeChanger={themeChanger} version={'Testing 123'}>
      <Demo>
        <Info></Info>
        <button
          type="button"
          class="btn btn-secondary"
          onClick={props.trpcTest}
        >
          Test TRPC
        </button>
      </Demo>
    </Drawer>
  )
}
