import { initTRPC } from '@trpc/server'
import { eq } from 'drizzle-orm/expressions'
import { z } from 'zod'
import { posts } from './schema'
import type { Context } from './_worker'

const t = initTRPC.context<Context>().create()

const publicProcedure = t.procedure
const router = t.router

const postRouter = router({
  createPost: publicProcedure
    .input(z.object({ title: z.string(), message: z.string() }))
    .mutation(({ ctx, input }) =>
      ctx.db
        .insert(posts)
        .values({
          userId: ctx.user.id,
          ...input,
        })
        .returning()
        .get()
    ),
  listPosts: publicProcedure.query(
    ({ ctx }) =>
      ctx.db.select().from(posts).where(eq(posts.userId, ctx.user.id)).all()
    // .leftJoin(posts, eq(posts.parentId, posts.id))
  ),
})

export const appRouter = router({
  post: postRouter,
  hello: publicProcedure.input(z.string().nullish()).query(({ ctx, input }) => {
    return `hello ${input ?? 'world'}`
  }),
})

export type AppRouter = typeof appRouter
