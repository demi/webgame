import { sendMessage } from '@/util'
;(async () => {
  const { install } = await import('@twind/core')
  const tailwindPreset = (await import('@twind/preset-tailwind')).default
  install({
    presets: [tailwindPreset()],

    // { hash: false } needed for daisy-ui which uses default tailwind css variables.
    hash: false,
  })
})()

const trpcPromise = import('@/trpc')
const trpcTest = async () => {
  const { test } = await trpcPromise
  test()
}

let refreshing = false
navigator.serviceWorker.addEventListener('controllerchange', () => {
  if (!refreshing) {
    // TODO: Show notification to refresh page.
    // window.location.href = '/'
    refreshing = true
  }
})

if (DEVELOPMENT) {
  new EventSource('/esbuild').addEventListener('change', (event) => {
    // Reload service worker on esbuild live reload event.
    // This will also reload the cache with index.html and _loader.ts
    sendMessage('SELF_DESTRUCT')
  })
}

interface CompiledCode {
  url: string
}

async function compileTsx(filename: string) {
  const r = await fetch(filename)
  if (!r.ok) throw `File ${filename} not found.`
  const tsx = await r.text()

  const data = await sendMessage<CompiledCode>({
    filename,
    tsx,
  })
  if (data.url) return import(data.url)
  else throw `Failed to compile ${filename}!`
}

const pagePromise = compileTsx('mods/page.tsx').then((v) => {
  const test = v as typeof import('!tsx/page')
  return test
})

;(async () => {
  const { createSignal } = await import('solid-js')
  const { render } = await import('solid-js/web')
  const { Page } = await pagePromise

  const [version, setVersion] = createSignal('')
  sendMessage<{ version: string }>('VERSION').then((info) => {
    setVersion(new Date(info.version).toLocaleString())
  })

  render(() => <Page trpcTest={trpcTest}></Page>, document.body)
})()
