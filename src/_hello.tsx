import { createResource, createSignal, lazy, Suspense } from 'solid-js'
import { render } from 'solid-js/web'

import { ThemeChange } from './mods/ThemeChange'
import type { LoaderType } from './_loader'

const fetchArchivePromise = fetch(__CACHE_ARCHIVE__)

if (DEVELOPMENT) {
  new EventSource('/esbuild').addEventListener('change', async (event) => {
    // sendMessage('SELF_DESTRUCT')
    await cachePromise
    await caches.delete(CACHE_NAME)
    window.location.href = '/'
  })
}

// NOTE: Prefetch doesn't seem to speed up loading, actually makes it slower
// if the resource is requested before prefetch completes on slow connections.
// <link rel="prefetch" href="/sw.js" />
// <link rel="prefetch" href="/_loader.js" />
const loaderUrl = '/_loader.js'

// const loaderPromise = import(loaderUrl)
// Append _loader script to avoid importing it as module,
// which would prevent importMap from being modified by document.write
const loaderPromise = new Promise<LoaderType>((resolve, reject) => {
  const script = document.createElement('script')
  script.type = 'application/javascript'
  script.src = loaderUrl
  script.onload = () => {
    if ('loader' in window) {
      resolve(window.loader as LoaderType)
      window.loader = undefined
    } else {
      reject()
    }
  }
  document.head.appendChild(script)
})
const cachePromise = loaderPromise.then((loader) =>
  Promise.all([loader.loadServiceWorker(), loader.load(fetchArchivePromise)])
)

if (window.location.search) {
  const params = new URLSearchParams(window.location.search)
  if (params.has('autoload')) {
    window.history.replaceState({}, document.title, window.location.pathname)
    console.log('Autoloading main...')
    cachePromise.then(async () => {
      const loader = await loaderPromise
      loader.loadGamePage()
    })
  }
} else {
  const themes = [
    {
      name: '🌝 Light',
      id: 'light',
    },
    {
      name: '🌚 Dark',
      id: 'dark',
    },
  ]
  const [loaderResource] = createResource(() =>
    loaderPromise.then(async (loader) => {
      await cachePromise
      return () => {
        dispose()
        loader.loadGamePage()
      }
    })
  )
  const [message, setMessage] = createSignal('')
  const LoaderInfo = () => {
    return (
      <div class="h-full w-full">
        <div class="card glass m-4 mt-16 w-80">
          <div class="card-body">
            <h1 class="card-title m-auto">Info</h1>
            <div>{message()}</div>
            <div class="card-actions justify-end">
              <Suspense
                fallback={
                  <button type="button" class="btn" disabled={true}>
                    Loading...
                  </button>
                }
              >
                <button
                  type="button"
                  class="btn btn-secondary"
                  onClick={loaderResource()}
                >
                  Load
                </button>
              </Suspense>
            </div>
          </div>
        </div>
      </div>
    )
  }

  const NavBar = () => {
    return (
      <nav class="navbar w-full md:gap-1 lg:gap-2">
        <div class="flex flex-1">
          <span
            class="tooltip tooltip-bottom before:text-xs before:content-[attr(data-tip)]"
            data-tip="Menu"
          >
            <label for="drawer" class="btn btn-square btn-ghost drawer-button ">
              <svg
                width="20"
                height="20"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                class="inline-block h-5 w-5 stroke-current md:h-6 md:w-6"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M4 6h16M4 12h16M4 18h16"
                ></path>
              </svg>
            </label>
          </span>
          <div class="flex items-center gap-2 ">
            <a
              href="/"
              aria-current="page"
              aria-label="Homepage"
              class="flex-0 btn btn-ghost px-2 "
            >
              <div class="font-titletext-primary inline-flex text-lg  transition-all duration-200 md:text-3xl">
                <span class="text-content normal-case">{document.title}</span>
              </div>
            </a>{' '}
            <a
              href="/docs/changelog"
              class="link link-hover font-mono text-xs text-opacity-50 "
            >
              <div class="hidden sm:block">
                <span data-tip="Changelog" class="tooltip tooltip-bottom">
                  Test???
                </span>
              </div>
            </a>
          </div>
        </div>

        <ThemeChange themes={themes}></ThemeChange>
      </nav>
    )
  }

  const Drawer = () => {
    return (
      <div
        class="drawer h-screen w-full overflow-y-hidden"
        style={{
          '--glass-reflex-opacity': 0,
        }}
      >
        <input id="drawer" type="checkbox" class="drawer-toggle" />
        <div class="drawer-content">
          <NavBar />
          <LoaderInfo />
        </div>
        <div class="drawer-side">
          <label for="drawer" class="drawer-overlay"></label>
          <aside class="glass card w-64 rounded">
            <ul class="menu text-base-content flex flex-col p-0 px-4">
              <li>
                <a>Item 1</a>
              </li>
              <li>
                <a>Item 2</a>
              </li>
              <li>
                <a>Item 3</a>
              </li>
            </ul>
          </aside>
        </div>
      </div>
    )
  }

  const dispose = render(Drawer, document.body)
}
