export type ManifestEntry = {
  path: string
  ext: string
  mime: string
  size: number
  offset: number
}
declare global {
  // https://stackoverflow.com/questions/59459312/using-globalthis-in-typescript
  // var used here to be able to assign to globalThis
  // eslint-disable-next-line no-var
  // var Alpine: AlpineType

  const CACHE_NAME: string
  const DEVELOPMENT: boolean
  const __SW_VERSION__: string
  const __CACHE_ARCHIVE__: string
  const __CACHE_MANIFEST__: { [key: string]: ManifestEntry }
}
// NOTE: export is needed here otherwise global declarations don't work
export {}
