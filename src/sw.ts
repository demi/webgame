declare let self: ServiceWorkerGlobalScope
// export default {}

// Use @babel/core instead of @babel/standalone since that includes
// a bunch of built-in plugins for es5, etc.
// import type { NodePath, PluginObj } from '@babel/core'
// import { transformSync } from '@babel/core'

import type { BabelFileResult, NodePath, PluginObj, Visitor } from '@babel/core'
import { transformSync } from '@babel/core'

import JSXDOMExpressions from 'babel-plugin-jsx-dom-expressions'
// import BabelTransformTypescript from '@babel/plugin-transform-typescript'
import presetTypescript from '@babel/preset-typescript'

import {
  Identifier,
  NumericLiteral,
  StringLiteral,
  VariableDeclarator,
  stringLiteral,
} from '@babel/types'

function firstParent(path: NodePath, type: string): NodePath | null {
  if (path == null || path.parentPath == null) return null
  if (path.parentPath.type == type) return path.parentPath
  return firstParent(path.parentPath, type)
}

const testVisitor: Visitor = {
  ObjectProperty(path) {
    const key = path.node.key
    // if (!t.isIdentifier(key)) return
    // const id: t.Identifier = key
    const id = key as Identifier
    const name = id.name
    if (name != 'diameter') return
    const parentPath = firstParent(path, 'VariableDeclarator')
    if (parentPath) {
      const decl = parentPath.node as VariableDeclarator
      const id = decl.id as Identifier
      if (id.name == 'otherSphere') {
        // console.log(`Diameter! :D ${id.name}`, path)
        const value = path.node.value as NumericLiteral
        value.value = 0.5 + 2 * Math.random()
      }
    }
  },
  // AssignmentExpression(path) {
  //   const lval = path.node.left
  //   console.log(`Assignment :D`, path)
  //   if (t.isIdentifier(lval)) {
  //     const identifier: t.Identifier = lval
  //     const name = identifier.name

  //     console.log(`Identifier :D ${name}`, path)
  //   } else if (t.isMemberExpression(lval)) {
  //     const expression: t.MemberExpression = lval
  //     const identifier: t.Identifier = lval.property
  //     const name = identifier.name

  //     console.log(`Expression :D ${name}`, path)
  //   }
  // },
}

const runtimeImportMap: { [key: string]: string } = {}
function mapPathString(nodePath: NodePath<StringLiteral>) {
  // if (!isStringLiteral(nodePath)) {
  //   return
  // }

  const sourcePath = nodePath.node.value
  if (!sourcePath.startsWith('!ts')) return
  const [_, extension, filename] = sourcePath.match(/!(ts.?)\/(.*)/)!
  const inputFile = `/mods/${filename}.${extension}`

  if (inputFile in runtimeImportMap) {
    // outputFile = `/mods/${filename}.${}`
    // nodePath.node.value = runtimeImportMap[inputFile]
    nodePath.replaceWith(stringLiteral(runtimeImportMap[inputFile]))
  } else {
    // Compile code async.
    // Babel traverse doesn't support async,
    ;(async () => {
      const cache = await caches.open(CACHE_NAME)
      const source = await cache.match(inputFile).then((r) => r!.text())

      let result: BabelFileResult | null = null
      switch (extension) {
        case 'tsx':
          result = compileSolid(inputFile, source)
          break
        case 'ts':
          result = compileTypescript(inputFile, source)
          break
      }

      if (result != null && result.code) {
        const url = await blobify(inputFile, result.code)
        // const outputFile = new URL(url).pathname
      }
    })()
    const outputFile = `/mods/${filename}.js`

    nodePath.replaceWith(stringLiteral(outputFile))

    runtimeImportMap[inputFile] = outputFile
  }
}

const importVisitor: Visitor = {
  ImportDeclaration(path) {
    mapPathString(path.get('source'))
  },
  // ExportDeclaration(path) {
  //   console.log('export visitor???', path)
  // },
}

const myPlugin = function (): PluginObj {
  return {
    visitor: {
      Program: {
        enter(path, state) {
          // const name = path.node
          path.traverse({
            ...testVisitor,
            ...importVisitor,
          })
        },
        exit(path, state) {
          path.traverse(importVisitor)
        },
      },
    },
  }
}
function compileTypescript(filename: string, source: string) {
  return transformSync(source, {
    filename,
    babelrc: false,
    compact: true,
    minified: true,
    presets: [presetTypescript],
    plugins: [myPlugin],
  })
}

function compileSolid(filename: string, source: string) {
  const compiled = transformSync(source, {
    filename,
    babelrc: false,
    compact: true,
    minified: true,
    comments: false,
    presets: [presetTypescript],
    plugins: [
      myPlugin,
      [
        JSXDOMExpressions,
        {
          moduleName: 'solid-js/web',
          builtIns: [
            'For',
            'Show',
            'Switch',
            'Match',
            'Suspense',
            'SuspenseList',
            'Portal',
            'Index',
            'Dynamic',
            'ErrorBoundary',
          ],
          contextToCustomElements: true,
          wrapConditionals: true,
          generate: 'dom',
        },
      ],
    ],
  })
  return compiled
}

async function blobify(filename: string, code: string) {
  // const blobData = new Blob([code], {
  //   type: 'text/javascript',
  // })
  // const url = URL.createObjectURL(blobData)
  // return url

  const cache = await caches.open(CACHE_NAME)

  const jsName = `${filename.split('.')[0]}.js`
  await cache.put(
    jsName,
    new Response(code, {
      headers: { 'Content-Type': 'text/javascript' },
    })
  )
  return new URL(jsName, location.origin).toString()
}

self.addEventListener('install', (event) => {
  // console.log('sw: Service worker installed...')
  // console.log('sw: install')
  // Immediately reload.
  // self.skipWaiting()
})
self.addEventListener('activate', (event) => {
  // console.log('sw: activate')
  // TODO: Is this needed?
  // event.waitUntil(self.clients.claim())
  // self.skipWaiting()
})

// self.addEventListener('push', (event) => {
//   console.log('sw: Service worker push...')
//   console.log(event)
// })

self.addEventListener('message', async (event) => {
  if (event.data && event.data.filename) {
    const port = event.ports[0]
    console.log('babel: onmessage', event)
    const data = event.data
    if (data.ts) {
      console.log('Compiling typescript...')
      const result = compileTypescript(data.filename, data.ts)
      if (result && result.code)
        port.postMessage({
          filename: data.filename,
          url: await blobify(data.filename, result.code),
        })
      else port.postMessage({ error: 'Babel error' })
    } else if (data.tsx) {
      console.log('Compiling tsx...')
      const result = compileSolid(data.filename, data.tsx)
      if (result && result.code)
        port.postMessage({
          filename: data.filename,
          url: await blobify(data.filename, result.code),
        })
      else port.postMessage({ error: 'Babel error' })
    } else {
      port.postMessage({ error: 'Invalid message' })
    }
  }
  switch (event.data) {
    case 'SKIP_WAITING':
      self.skipWaiting()
      break
    case 'CLAIM_ME':
      event.waitUntil(self.clients.claim())
      break
    case 'VERSION':
      event.ports[0].postMessage({ version: __SW_VERSION__ })
      break
    case 'SELF_DESTRUCT':
      console.log('sw: selfdestruct!')
      self.registration
        .unregister()
        .then(function () {
          return self.clients.matchAll()
        })
        .then(function (clients) {
          clients.forEach((client) => {
            const url = new URL(client.url)
            return client.navigate(`${url.pathname}?autoload=true`)
          })
        })
      break
  }
})

// let cache: Cache
// caches.open(CACHE_NAME).then((c) => (cache = c))
self.addEventListener('fetch', (event: FetchEvent) => {
  const request = event.request
  const url = new URL(request.url)

  // worker trpc api
  if (url.pathname.startsWith('/trpc/')) return

  event.respondWith(
    caches.match(request).then((response) => {
      // TODO: Send message to client to reload page?
      // If request not in cache, that means cache was cleared and neads
      // to be reloaded in _loader.js
      if (!response) {
        console.warn(`sw: Response not in cache for ${url.pathname}`)
        return fetch(request).then((response) => {
          // Dynamically cache .png icons
          if (url.pathname.endsWith('.png') && response.ok) {
            const copy = response.clone()
            caches.open(CACHE_NAME).then((c) => c.put(request, copy))
          }
          return response
        })
      }
      return response
    })
  )
})
