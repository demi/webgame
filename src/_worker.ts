import {
  FetchCreateContextFnOptions,
  fetchRequestHandler,
} from '@trpc/server/adapters/fetch'
import { appRouter } from '@/router'

import type { DrizzleD1Database } from 'drizzle-orm/d1'
import { drizzle } from 'drizzle-orm/d1'
import { eq } from 'drizzle-orm/expressions'

import { users } from './schema'
import { InferModel } from 'drizzle-orm'

export interface Env {
  DB: D1Database
}

export type User = InferModel<typeof users, 'select'>

export type Context = {
  req: Request
  resHeaders: Headers
  db: DrizzleD1Database
  user: User
}

export default {
  async fetch(
    request: Request,
    env: Env,
    ctx: ExecutionContext
  ): Promise<Response> {
    async function createContext({
      req,
      resHeaders,
    }: FetchCreateContextFnOptions) {
      const db = drizzle(env.DB)

      const id = 1

      let user = await db.select().from(users).where(eq(users.id, id)).get()
      if (!user) {
        user = await db
          .insert(users)
          .values({
            name: 'test',
            email: 'test@test.test',
          })
          .returning()
          .get()
      }

      return { req, resHeaders, db, user }
    }

    return fetchRequestHandler({
      endpoint: '/trpc',
      req: request,
      router: appRouter,
      createContext,
    })
  },
}
