// Fake require('path') functions for babel.
export function resolve(base, path) {
  return path ?? base
}
export function relative(base, path) {
  return path ?? base
}
export function basename(v) {
  const arr = v.split('/')
  return arr[arr.length - 1]
}
export function extname(v) {
  const arr = v.split('.')
  return arr[arr.length - 1]
}

export default {
  resolve,
  relative,
  basename,
  extname,
}
