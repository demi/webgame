### Character Creation

#### Stat system.

1. 3 stats, Phys, Int, Social
2. 0 - 5 star ratings.
3. 3 points to distribute at start.
4. 0 stars = bad, apply penalty.
5. 1 star = average, base level.
6. 2 stars = stat increase + bonus items/etc. on start.
7. 3 stars = exceptional. Large stat increase, but applies negative as well.
8. 4 + 5 stars = superhuman, locked behind in-game progression, achievements.
